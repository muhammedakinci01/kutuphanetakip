﻿using KutuphaneTakip.Models;
using Microsoft.EntityFrameworkCore;

namespace KutuphaneTakip.Repositories
{
    public class RepositoryContext : DbContext
    {
        public RepositoryContext(DbContextOptions options) :
        base(options)
            {
            
        }
        public DbSet<KullaniciDto> Kullanicis { get; set; }
        public DbSet<Adres> Adress { get; set; }
        public DbSet<DilTurleriDto> DilTurleris { get; set; }
        public DbSet<ErisebilirlikDto> Erisebilirliks { get; set; }
        public DbSet<GeriBildirimDto> GeriBildirims { get; set; }
        public DbSet<KitaplarDto> Kitaplars { get; set; }
        public DbSet<KitapTurleriDto> KitapTurleris { get; set; }
        public DbSet<OdalarDto> Odalars { get; set; }
        public DbSet<OduncDurumlariDto> OduncDurumlaris { get; set; }
        public DbSet<OduncVerilenAyarDto> OduncVerilenAyars { get; set; }
        public DbSet<OduncVerilenKitaplarDto> OduncVerilenKitaplars { get; set; }
        public DbSet<RezervasyonDto> Rezervasyons { get; set; }
        public DbSet<YayineviDto> Yayinevis { get; set; }
        public DbSet<YazarDto> Yazars { get; set; }
        public DbSet<DepartmanlarDto> Departmanlars { get; set; }
        public DbSet<KutuphaneDto> Kutuphanelers { get; set; }
        public DbSet<OdaTipiDto> OdaTipis { get; set; }
        public DbSet<KullaniciTipiDto> KullaniciTipis { get; set; }

    }
}
