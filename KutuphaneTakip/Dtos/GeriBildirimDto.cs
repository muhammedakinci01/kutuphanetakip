﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class GeriBildirimDto
    {
        public int KullaniciId { get; set; }

        public string Gmail { get; set; }
        public int Sms { get; set; }
        public string Bildiri { get; set; }
        public int Tarih { get; set; }
    }
}
