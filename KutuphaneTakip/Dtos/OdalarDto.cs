﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class OdalarDto
    {
        public int KutuphaneId { get; set; }

        public int OdatipId { get; set; }


        public string OdaAdi { get; set; }


    }
}
