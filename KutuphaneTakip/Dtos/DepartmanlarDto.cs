﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class DepartmanlarDto
    {
        public int KutuphaneId { get; set; }


        public string Departman { get; set; }

    }
}
