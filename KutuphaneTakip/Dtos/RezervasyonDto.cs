﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class RezervasyonDto
    {
        public int KutuphaneId { get; set; }

        public int KullaniciId { get; set; }

        public string KullaniciAdi { get; set; }
        public string Gmail { get; set; }
        public int Telefon { get; set; }
        public int Gun { get; set; }
        public int Saat { get; set; }
        public int Masa { get; set; }
        public int Oda { get; set; }
    }
}
