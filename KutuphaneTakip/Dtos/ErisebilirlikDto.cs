﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class ErisebilirlikDto
    {
        public int Yetki { get; set; }
    }
}
