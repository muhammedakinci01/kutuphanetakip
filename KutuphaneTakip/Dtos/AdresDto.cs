﻿namespace KutuphaneTakip.Dtos
{
    public class AdresDto
    {
        public string Il { get; set; }
        public string Ilce { get; set; }
        public string Mahalle { get; set; }
        public string Sokak { get; set; }
        public int Bina { get; set; }
        public int Kat { get; set; }
    }
}
