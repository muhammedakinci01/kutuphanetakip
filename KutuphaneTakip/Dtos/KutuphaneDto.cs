﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class KutuphaneDto
    {
        public string KutuphaneAdi { get; set; }
    }
}
