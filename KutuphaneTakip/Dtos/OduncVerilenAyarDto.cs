﻿namespace KutuphaneTakip.Dtos
{
    public class OduncVerilenAyarDto
    {
        public int UzatmaSiniri { get; set; }
        public int GetirmeSiniri { get; set; }
        public int KutuphaneId { get; set; }
    }
}
