﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Dtos
{
    public class KullaniciDto
    {
        public int AdresId { get; set; }
        public string Gmail { get; set; }
        public int Telefon { get; set; }
        public int ErisebilirlikId { get; set; }
        public int DepartmanId { get; set; }

        public int KullaniciTipiId { get; set; }

    }
}
