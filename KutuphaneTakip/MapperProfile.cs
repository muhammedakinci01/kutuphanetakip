﻿using AutoMapper;
using KutuphaneTakip.Dtos;
using KutuphaneTakip.Models;

namespace KutuphaneTakip
{
    public class MapperProfile : Profile
    {
        public MapperProfile() {
   
           CreateMap<Adres,AdresDto>();

           CreateMap<AdresDto,Adres>();

           CreateMap<Departmanlar,DepartmanlarDto>();

          CreateMap<DepartmanlarDto,Departmanlar>();

           CreateMap<DilTurleri, DilTurleriDto>();

           CreateMap<DilTurleriDto,DilTurleri>();

           CreateMap<Erisebilirlik,ErisebilirlikDto>();

           CreateMap<ErisebilirlikDto,Erisebilirlik>();

            CreateMap<GeriBildirim, GeriBildirimDto>();

            CreateMap<GeriBildirimDto, GeriBildirim>();

            CreateMap<Kitaplar, KitaplarDto>();


            CreateMap<KitaplarDto, Kitaplar>();
            CreateMap<KitapTurleri, KitapTurleriDto>();

            CreateMap<KitapTurleriDto, KitapTurleri>();

          CreateMap<Kullanici, KullaniciDto>();

          CreateMap<KullaniciDto, Kullanici>();

            CreateMap<KullaniciTipi, KullaniciTipiDto>();

            CreateMap<KullaniciTipiDto, KullaniciTipi>();

          CreateMap<Kutuphane, KutuphaneDto>();

          CreateMap<KutuphaneDto, Kutuphane>();

            CreateMap<Odalar, OdalarDto>();

            CreateMap<OdalarDto, Odalar>();

            CreateMap<OdaTipi, OdaTipiDto>();

            CreateMap<OdaTipiDto, OdaTipi>();

          CreateMap<OduncDurumlari, OduncDurumlariDto>();

          CreateMap<OduncDurumlariDto, OduncDurumlari>();

          CreateMap<OduncVerilenAyar, OduncVerilenAyarDto>();

          CreateMap<OduncVerilenAyarDto, OduncVerilenAyar>();

          CreateMap<OduncVerilenKitaplar, OduncVerilenKitaplarDto>();

          CreateMap<OduncVerilenKitaplarDto, OduncVerilenKitaplar>();

          CreateMap<Rezervasyon, RezervasyonDto>();

          CreateMap<RezervasyonDto, Rezervasyon>();

          CreateMap<Yayinevi, YayineviDto>();

          CreateMap<YayineviDto, Yayinevi>();

          CreateMap<Yazar, YazarDto>();

          CreateMap<YazarDto, Yazar>();


        }
    }
}
