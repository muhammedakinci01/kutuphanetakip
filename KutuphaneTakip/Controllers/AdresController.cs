﻿using AutoMapper;
using KutuphaneTakip.Dtos;
using KutuphaneTakip.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace KutuphaneTakip.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdresController : ControllerBase
    {
        private readonly IMapper _mapper;
        private static List<Adres> adress = new List<Adres>();

        public AdresController(IMapper mapper)
        {
            _mapper = mapper;
        }
        [HttpGet]
        public IActionResult AdresGetAll() {
            try
            {
               return Ok(adress.Select(adres => _mapper.Map<AdresDto>(adres)));
                
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        [HttpPost]
        public IActionResult AdresPost(AdresDto adresDto)
        {

            Adres adres = _mapper.Map<Adres>(adresDto);
            adress.Add(adres);
            return Ok(adres);
        }
    }
}
