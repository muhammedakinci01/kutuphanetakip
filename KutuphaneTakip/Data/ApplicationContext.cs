﻿using KutuphaneTakip.Models;

namespace KutuphaneTakip.Data
{
    public static class ApplicationContext
    {


        public static List<Adres> Adress { get; set; }
        public static List<DepartmanlarDto> Departmanlars { get; set; }
        public static List<DilTurleriDto> DilTurleris { get; set; }
        public static List<ErisebilirlikDto> Erisebilirliks { get; set; }
        public static List<GeriBildirimDto> GeriBildirims { get; set; }
        public static List<KitaplarDto> Kitaplars { get; set; }
        public static List<KitapTurleriDto> KitapTurleris { get; set; }
        public static List<KullaniciDto> Kullanicis { get; set; }
        public static List<KullaniciTipiDto> KullaniciTipis { get; set; }
        public static List<KutuphaneDto> Kutuphanes { get; set; }
        public static List<OdalarDto> Odalars{ get; set; }
        public static List<OdaTipiDto> OdaTipis{ get; set; }
        public static List<OduncDurumlariDto> OduncDurumlaris{ get; set; }
        public static List<OduncVerilenAyarDto> OduncVerilenAyars{ get; set; }
        public static List<OduncVerilenKitaplarDto> OduncVerilenKitaplars{ get; set; }
        public static List<RezervasyonDto> Rezervasyons{ get; set; }
        public static List<YayineviDto> Yayinevis{ get; set; }
        public static List<YazarDto> Yazars{ get; set; }
    }
}
